<?php
namespace App\Model;
use Nette;
use Nette\Security;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;
class Autheticator implements Nette\Security\IAuthenticator
{	private $database;
	function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}
	function authenticate(array $user_info)
	{
		$name = $user_info[0];
		$password = $user_info[1];
		$record = $this->database->table('uzivatelia')->where('meno', $name)->fetch();
		if ($record == NULL || strcmp($password, $record->heslo) != 0)
		{
			throw new AuthenticationException('Chybná kombinácia mena a hesla.');
		} else {
			return new Identity($record->meno, $record->rola, array('meno' => $record->meno));
		}
	}
}
