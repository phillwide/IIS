<?php
namespace App\Forms;
use Nette;
use Nette\Application\UI\Form;


class MoreButton extends Nette\Object 
{
	private $database;
	private $Id;
	private $table;

	public function __construct(Nette\Database\Context $databaza, $Id, $table)
	{
		$this->database = $databaza;
		$this->Id = $Id; 
		$this->table = $table; 
	}

	public function create()
	{
		$form = new Form;
		$form->addSubmit('More', 'Více');
		$form->onSuccess[] = array($this, 'succ');
		return $form;
	}

	public function succ(Form $form, $values)
	{
		$form->getPresenter()->redirect($this->table, $this->Id);
	}

}