<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use App\Forms\CreateMatchForm;
use App\Forms\MoreButton;
use App\Forms\EditMatchForm;

class MatchPresenter extends BasePresenter
{
	private $database;
	private $id;
	private $factory;

	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}

	 public function renderShow()
	{
		if(!$this->getUser()->isLoggedIn())
		{
			$this->redirect('Sign:in');
		}
		$this->template->ucast = array();
		$i = 0;
		$this->template->zapasy = $this->database->table('zapas');
	}

	public function actionDefault()
	{
		$this->redirect('Match:Show');
	}

	protected function createComponentCreateForm()

	{
		$form = (new CreateMatchForm($this->database, $this));
		return $form->create();

	}


	public function renderMore($id)

	{
		if(!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:in');
		}

		$this->template->zapas = $this->database->table('zapas')->get($id);
		$this->template->udalosti = $this->database->query('SELECT * FROM udalost NATURAL JOIN hrac NATURAL JOIN rozhodci WHERE ID_zapasu='.$id);
		$this->template->rozhodcovia = $this->database->query('SELECT * FROM je_pod_dozorom NATURAL JOIN rozhodci WHERE ID_zapasu = ' . $id);
		/*foreach ($udalosti as $key => $value) {
			print('here');
			var_dump($value);
			foreach ($this->template->rozhodcovia as $key2 => $value2) {
				if ($value2['ID_rozhodcu'] == $value['ID_rozhodcu'])
				{
					$this->template->udalosti['jmeno_R'] = $value2['ID_rozhodcu'];
				}
			}
		}*/
	}

	protected function createComponentEditMatchForm()
	{
		$this->factory = new EditMatchForm($this->database, $this->id);
		$form = $this->factory->create();
		return $form;
	}

	public function actionMore($id)

	{
		$this->id = $id;
		$record = $this->database->table('zapas')->get($id);
		$record = $record->toArray();
		$bool1 = false;
		$bool2 = false;
		$bool3 = false;

		$rozhodci = $this->database->query('SELECT * FROM rozhodci NATURAL JOIN je_pod_dozorom WHERE ID_zapasu='.$id);
		foreach ($rozhodci as $key => $value) {
			if ($value['pozicia'] == 'H')
			{
				$record['jmenoH'] = $value['jmeno_R'];
			}
			else if ($value['pozicia'] == 'V' and $bool1 == false)
			{
				$record['jmenoV1'] = $value['jmeno_R'];
				$bool1 = true;
			}
			else if ($value['pozicia'] == 'V' and $bool2 == false)
			{
				$record['jmenoV2'] = $value['jmeno_R'];
				$bool2 = true;
			}
			else if ($value['pozicia'] == 'V' and $bool3 == false)
			{
				$record['jmenoV3'] = $value['jmeno_R'];
				$bool3 = true;
			}
		}
	
		$this['editForm']->setDefaults($record);
		$this->factory->id = $id;
	}



	protected function createComponentDeleteButton()
	{
		$form = new Form;
		$form->addSubmit('vymazat', 'Vymazat')->setAttribute('class', 'btn btn-danger');;
		$form->onSuccess[] = array($this, 'succDeleteButton');

		return $form;
	}

	public function actionDeleteMatch($id)
	{
		if ($this->getUser()->identity->roles[0] == 'admin') {
			$this->database->table('udalost')->where('ID_zapasu', $id)->delete();

			$this->database->table('je_pod_dozorom')->where('ID_zapasu', $id)->delete();
			$this->database->table('zapas')->where('ID_zapasu', $id)->delete();
		
			$this->presenter->flashMessage('Odstránené', 'alert alert-success alert-dismissible');
		} else {
			$this->presenter->flashMessage('Neoprávněná akce', 'alert alert-warning alert-dismissible');
		}
		$this->redirect('Match:Show');
	}

	protected function createComponentEditForm()
	{
		$this->factory = new EditMatchForm($this->database, $this->id);
		$form = $this->factory->create();
		return $form;
	}

	public function actionDeleteEvent($match, $id)
	{
		if ($this->getUser()->identity->roles[0] == 'admin') {
			$ret = $this->database->table('udalost')->where('ID_udalosti', $id)->delete();
			if ($ret != 0) {
				$this->presenter->flashMessage('Odstránené', 'alert alert-success alert-dismissible');
			} else {
				$this->presenter->flashMessage('Špatné ID', 'alert alert-danger alert-dismissible');
			}
		} else {
			$this->presenter->flashMessage('Neoprávněná akce', 'alert alert-warning alert-dismissible');
		}

		$this->redirect('Match:More',  $match);
	}
	
	protected function createComponentAddEvent($id)
	{
		
		$id = $this->id;

		$values = array();
		$values_ref = array();

		$match = $this->database->table('zapas')->get($id);
		$match = $match->toArray();

		$result_ref = $this->database->query('SELECT * FROM rozhodci NATURAL JOIN je_pod_dozorom WHERE ID_zapasu= '.$id);
		foreach ($result_ref as $res_ref)
		{
			$values_ref[$res_ref->jmeno_R] = $res_ref->jmeno_R;
		}
		$var = $match['domaci'];
		$result = $this->database->query('SELECT * FROM hrac');
		
		foreach ($result as $res)
		{
			if ($res['ID_tym'] == $match['domaci'] or $res['ID_tym'] == $match['hostia'])
				$values[$res->rodne_cislo] = $res->jmeno_H;
		}

		$form = new Form;

		$renderer = $form->getRenderer();

 		$renderer->wrappers['error']['container'] = 'div class="alert alert-danger alert-dismissible"';
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
 		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$form->addInteger('cas', '*Minuta')->setRequired();
		$form->addText('druh', '*Popis Události')->setRequired();
		$form->addSelect('rodne_cislo', '*Hráč', $values)->setRequired()->setAttribute("class", "form-control");;
		$form->addSelect('jmeno_R', '*Rozhodčí', $values_ref)->setRequired()->setAttribute("class", "form-control");;
		$form->addSubmit('add', 'Přidat');
		$form->onSuccess[] = array($this, 'succEvent');
		return $form;
	}


	public function succEvent(Form $form, $values)
	{
		$record = $this->database->query('SELECT * FROM rozhodci');

	
		$record = $this->database->query('SELECT * FROM hrac WHERE jmeno_H='.$values['rodne_cislo']);
		foreach ($record as $key => $value) {
			$values['rodne_cislo'] = $value['rodne_cislo'];
		}
		$values['ID_zapasu'] = $this->id;
		$this->database->table('udalost')->insert($values);
		$form->getPresenter()->flashMessage('Přidáno', 'alert alert-success alert-dismissible');
		$form->getPresenter()->redirect('Match:More', $this->id);
	}
}