<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;


class CreateTeamForm extends Nette\Object
{
	private $database;

	public function __construct(Nette\Database\Context $databaza)
	{
		$this->database = $databaza;
	}

	public function create()
	{
		$form = new Form;

		$renderer = $form->getRenderer();
	 	$renderer->wrappers['error']['container'] = 'div class="alert alert-danger alert-dismissible"';	
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$skupiny = array();
		$skupiny['A'] = 'A';
		$skupiny['B'] = 'B';
		$skupiny['C'] = 'C';
		$skupiny['D'] = 'D';
		$form->addText('ID_tym', '*Skratka týmu')->addRule(Form::LENGTH,"Špatně", 3)->setRequired();
		$form->addText('trener', 'Hlavní trenér');
		$form->addText('asistenti', 'Asistent');
		$form->addSelect('skupina', 'Skupina', $skupiny);
		$form->addSubmit('ok', 'Přidat');
		$form->onSuccess[] = array($this, 'succ');
		return $form;

	}

	public function succ(Form $form, $hodnoty)
	{
	
		$this->database->table('tym')->insert($hodnoty);
		$form->getPresenter()->flashMessage('Úspěšne přidané!', 'alert alert-success alert-dismissible');

		$form->getPresenter()->redirect('Team:Show');
	}
}