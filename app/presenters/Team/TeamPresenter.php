<?php

namespace App\Presenters;

use Nette;

use Nette\Application\UI\Form;

use Nette\Application\UI\Multiplier;

use App\Forms\AddMatch;

use App\Forms\MoreButton;

use App\Forms\EditTeamForm;
use App\Forms\CreateTeamForm;



class TeamPresenter extends BasePresenter

{

	private $database;

	private $id;

	private $factory;

	private $rodne_cislo;





	public function __construct(Nette\Database\Context $databaza)

	{

		$this->database = $databaza;

	}



	public function renderShow()

	{
		 if(!$this->getUser()->isLoggedIn())
		{
			$this->redirect('Sign:in');
		}
		 $this->template->tymy = $this->database->table('tym');

	}



	protected function createComponentCreateForm()

	{
		$form = (new CreateTeamForm($this->database, $this));
		return $form->create();

	}

	protected function createComponentEditForm()
	{
		$this->factory = new EditTeamForm($this->database, $this->id);
		$form = $this->factory->create();
		return $form;
	}

	public function actionDefault()
	{
		$this->redirect('Team:Show');
	}

	public function renderMore($id)
	{
		if(!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:in');
		}

		$this->template->tym = $this->database->table('tym')->get($id);
		$hraci = $this->database->query('SELECT * FROM hrac');
		$new = array();
		$i = 0;
		foreach ($hraci as $key => $value) {
			if ($value['ID_tym'] == $id)
			{
				$new[$i] = $value;
				$i++;
			}
		}
		$this->template->hraci = $new;
	}

	public function actionEdit($rc, $id)
	{
		$this->id = $id;
		$this->rodne_cislo = $rc;
		if(!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:in');
		}

	}
	protected function createComponentEditTeamForm()
	{
		$this->factory = new EditTeamForm($this->database, $this->id);
		$form = $this->factory->create();
		return $form;
	}

	public function actionMore($id)
	{
		$this->id = $id;
		$record = $this->database->table('tym')->get($id);
		$record = $record->toArray();
		$hraci = $this->database->query('SELECT * FROM hrac');
		$new = array();
		$i = 0;
		foreach ($hraci as $key => $value) {
			if ($value['ID_tym'] == $id)
			{
				$new[$i] = $value;
				$i++;
			}
		}
		$this->template->hraci = $new;
		$this['editForm']->setDefaults($record);
		$this->factory->id = $id;
	}

	protected function createComponentEditPlayerForm()
	{
		$record = $this->database->table('hrac')->get($this->rodne_cislo);
		$form = new Form;
		$renderer = $form->getRenderer();

 		$renderer->wrappers['error']['container'] = 'div class="alert alert-danger alert-dismissible"';
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
 		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$positions = array();
		$positions['LK'] = 'LK';
		$positions['PK'] = 'PK';
		$positions['CR'] = 'CR';
		$positions['GK'] = 'GK';
		$positions['D'] = 'D';
		$form->addInteger('rodne_cislo', '*Rodní číslo')->setRequired()->addRule(Form::LENGTH,'Špatné rodní číslo', 10);
		$form->addText('jmeno_H', '*Jméno')->setRequired();
		$form->addText('klub', 'Klub');
		$form->addSelect('pozice', 'Pozice', $positions)->setAttribute("class", "form-control");;
		$form->addInteger('cislo_dresu', 'Číslo dresu');
		$form->addInteger('vek', 'Věk');
		$form->addSubmit('add', 'Editovat');
		$form->onSuccess[] = array($this, 'succEditPlayer');
		$form->setDefaults($record);
		return $form;
	}

	public function succEditPlayer(Form $form, $values)
	{

		$values['ID_tym'] = $this->id;

		
		/*$events = $this->database->query('SELECT * FROM udalost');
		foreach ($events as $key => $value) {
			if ($value['rodne_cislo'] == $this->rodne_cislo)
			{
				$value['rodne_cislo'] = $values['rodne_cislo'];
				$this->database->table('udalost')->get($value['ID_udalosti'])->update($value);
			}
		}*/
		$this->database->table('hrac')->get($this->rodne_cislo)->update($values);
		$form->getPresenter()->flashMessage('Editovat', 'alert alert-success alert-dismissible');
		$form->getPresenter()->redirect('Team:More', $this->id);
	}
	protected function createComponentDeleteButton()
	{
		$form = new Form;
		$form->addSubmit('vymazat', 'Vymazat')->setAttribute('class', 'btn btn-danger');;
		$form->onSuccess[] = array($this, 'succDeleteButton');

		return $form;
	}

	public function actionDeleteTeam($id)
	{
		if ($this->getUser()->identity->roles[0] == 'admin') {
			$this->database->table('hrac')->where('ID_tym', $id)->delete();
			$this->database->table('zapas')->where('domaci', $id)->delete();
			$this->database->table('zapas')->where('hostia', $id)->delete();
			$this->database->table('tym')->get($id)->delete();
		
			$this->presenter->flashMessage('Odstránené', 'alert alert-success alert-dismissible');
		} else {
			$this->presenter->flashMessage('Neoprávněná akce', 'alert alert-warning alert-dismissible');
		}
		$this->redirect('Team:Show');
	}
	public function actionDeletePlayer($team, $id)
	{
		if ($this->getUser()->identity->roles[0] == 'admin') {
			$this->database->table('udalost')->where('rodne_cislo', $id)->delete();
			$ret = $this->database->table('hrac')->where('rodne_cislo', $id)->delete();
			if ($ret != 0) {
				$this->presenter->flashMessage('Odstránené', 'alert alert-success alert-dismissible');
			} else {
				$this->presenter->flashMessage('Špatné ID', 'alert alert-danger alert-dismissible');
			}
		} else {
			$this->presenter->flashMessage('Neoprávněná akce', 'alert alert-warning alert-dismissible');
		}

		$this->redirect('Team:More',  $team);
	}
	protected function createComponentAddPlayer($id)
	{
		
		$id = $this->id;

		
		$form = new Form;

		$renderer = $form->getRenderer();

 		$renderer->wrappers['error']['container'] = 'div class="alert alert-danger alert-dismissible"';
		$renderer->wrappers['controls']['container'] = null;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
 		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.password'] = 'form-control';
		$renderer->wrappers['control']['.file'] = 'form-control';
		$renderer->wrappers['control']['.email'] = 'form-control';
		$renderer->wrappers['control']['.number'] = 'form-control';
		$renderer->wrappers['control']['.button'] = 'btn btn-primary';
		$renderer->wrappers['control']['.submit'] = 'btn btn-primary';

		$positions = array();
		$positions['LK'] = 'LK';
		$positions['PK'] = 'PK';
		$positions['CR'] = 'CR';
		$positions['GK'] = 'GK';
		$positions['D'] = 'D';
		$form->addInteger('rodne_cislo', '*Rodní číslo')->setRequired()->addRule(Form::LENGTH,'Špatné rodní číslo', 10);
		$form->addText('jmeno_H', '*Jméno')->setRequired();
		$form->addText('klub', 'Klub');
		$form->addSelect('pozice', 'Pozice', $positions)->setAttribute("class", "form-control");;
		$form->addInteger('cislo_dresu', 'Číslo dresu');
		$form->addInteger('vek', 'Věk');
		$form->addSubmit('add', 'Přidat');
		$form->onSuccess[] = array($this, 'succPlayer');
		return $form;
	}
	
	public function succPlayer(Form $form, $values)
	{
		$values['ID_tym'] = $this->id;
		$this->database->table('hrac')->insert($values);
		$form->getPresenter()->flashMessage('Přidáno', 'alert alert-success alert-dismissible');
		$form->getPresenter()->redirect('Team:More', $this->id);
	}

}